************
Introduction
************

The release team is delegated authority by the Debian Project Leader to oversee
and manage the releases of the ``testing``, ``stable``, and ``oldstable``
suites.

Audience
========

This handbook is aimed at readers who wish to know more about how the Release
Team is structured, how members go about their daily work and the processes
by which it achieves its objectives. It also functions as a procedural
reference for team members.

Objectives
==========

The release team's primary objectives, as described in the delegation, are:

* to decide on the release schedule (e.g.; freeze date, release date for
  stable releases and point-releases for ``stable`` and ``oldstable``)
* to define the content of the suites listed above, that is:

  * to define the packages that are part of those suites
  * to define the ports (architectures) that are part of those suites,

* to decide on the codenames for stable releases.
* to co-ordinate the work on the release notes, and have the final say
  on their content

Release Team members also have the final say on the official material
for each release (e.g., they decide which CD images are official ones).

This handbook is a guide to the tools, processes and approaches for achieving
these goals.

Who's Who
=========

The release team consists of three principal groups.

Release Managers
----------------

Release Managers are nominated internally and generally oversee development
of the next stable release. Following release, Release Managers are not
automatically expected to move to stable release management, but the event
is often a good opportunity to do so for team members who wish to reduce
their time commitment.

Stable Release Managers
-----------------------

Stable Release Managers are also known as SRMs, and oversee maintenance of
supported releases (usually ``stable`` and ``oldstable``).

Release Assistants
------------------

Release Assistants are expected to assist both Release Managers and, from time
to time as required, SRMs.
