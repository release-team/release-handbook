=====================
The ``testing`` suite
=====================

.. toctree::
   :maxdepth: 2

   migration
   transitions
   releaseday

The ``testing`` suite starts each release cycle as a copy of ``sid``, and it
is here that the next release is assembled. During the release process,
all the stable suites are demoted by one level and ``testing`` becomes the new
``stable`` suite.
