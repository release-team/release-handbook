=====================================
Migration from ``sid`` to ``testing``
=====================================

Automatic migration
===================

``britney``
-----------

This program is run periodically with input from Debian's Bug Tracking System,
installability information, and hint files to evaluate packages which are
ready to migrate to ``testing``.

The ``britney`` documentation provides more detailed information about
`how packages are evaluated <https://release-team.pages.debian.net/britney2/docs/short-intro-to-migrations.html>`_.

Direct uploads
==============
