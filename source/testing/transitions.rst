===========
Transitions
===========

A transition is the process required to manage a change in a library in ``sid``
which requires rebuilding of programs linked against it. To ensure that
``testing`` remains installable during this process, migration of both the
library and dependent packages is done in a managed way:

 * the library is first uploaded to ``sid`` with a new binary package
   representing the new library API/ABI
 * packages depending on the old library are rebuilt against the new
   ABI, or sourceful uploads are made to update programs for an incompatible
   API change
 * the library and rebuilt packages are allowed to migrate to ``testing``
   as they become eligible
 * the old library packages are cleaned up from ``sid`` and ``testing`` when
   they are no more reverse dependencies on them

A number of tools are maintained by the Release Team to make this process
easier to manage.

Transition tracker
------------------

Auto-hinter
-----------
