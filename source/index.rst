.. Release Team Handbook documentation master file, created by
   sphinx-quickstart on Sat Nov 25 16:02:31 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Release Team Handbook
=====================

.. warning::
   This handbook is in draft form, and may be incomplete, misleading or
   contain errors.

Contents:

.. toctree::
   :maxdepth: 3

   intro
   testing/testing
   stable/stable

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

